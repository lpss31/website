+++
title = 'Resume IN PROGRESS'
date = 2024-04-29T19:36:48+02:00
draft = true
+++

Education
---------

2021-2025 (expected)
:   **BSc, Computer Engineering**; Kyiv Polytechnic University

Experience
----------

2022-2024
**Working Student QA Automation: Digital Charging Solutions**

Had an amazing experience of working in a high demand infrastructure team, that was responsible for deployment, maintenance and development of our infrastructure.

I had an experience of working with:

:   **- Terraform**
:   **- Sonarqube**
:   **- Kubernetes**
:   **- Azure / Azure DevOps / Azure Pipelines**
:   **- Python scripting**

Technical Experience
--------------------
Technologies
:  **OSINT:**

:  **Linux**

:  **Networking**


Programming Languages
:   **C:** Experience in using C to develop everything from HTTP server to Linux Kernel modules. My recent projects has been implementation of memory allocator, and filesystem

:   **Python:** Work experience in using Python to write scripts to automate workflow.

:   Basic knowledge of **x86 assembly**, **ARM assembly**, **Rust**, **Go**


Current Learning Path
----------------------------------------
At the moment I am learning basic cybersecurity material at the [HackTheBox](https://academy.hackthebox.com). At the same time I am also applying and upgrading my Assembly knowledge while learning Reverse Engineering.

Spoken Languages
----------------------------------------

Human Languages:

:   **English (Fluent)**
:   **Ukrainian (Native)**
:   **German (B1)**
